import cv2 as cv
import numpy as np
from keras import models
from PIL import Image

video = cv.VideoCapture(0)
model = models.load_model('Tensorflow_models/cnn_25_epochs_1.h5')

if not video.isOpened():
    print("Cannot open camera")
    exit()
while True:
    # Capture frame-by-frame
    ret, frame = video.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break

    # Our operations on the frame come here
    # Convert the captured frame into RGB
    im = Image.fromarray(frame, 'RGB')

    # Resizing into dimensions you used while training
    im = im.resize((64, 64))
    img_array = np.array(im)

    # Expand dimensions to match the 4D Tensor shape.
    img_array = np.expand_dims(img_array, axis=0)

    # Calling the predict function using keras
    prediction = model.predict(img_array)  # [0][0]
    print(prediction)

    # Predictions
    if (prediction == 0):
        print("Man")
    elif (prediction == 1):
        print("Woman")

    # Display the resulting frame
    cv.imshow('Prediction', frame)
    if cv.waitKey(1) == ord('q'):
        break

# When everything done, release the capture
video.release()
cv.destroyAllWindows()