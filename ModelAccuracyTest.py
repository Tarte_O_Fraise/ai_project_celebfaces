import tensorflow as tf
from keras.models import load_model

BATCH_SIZE=128
IMG_SIZE = (64,64)

testing_set = tf.keras.utils.image_dataset_from_directory('./dataset/Testing',
                                                                 shuffle=True,
                                                                 batch_size=BATCH_SIZE,
                                                                 image_size=(IMG_SIZE))

classifier_cnn = load_model('Tensorflow_models/cnn_25_epochs_1.h5')

# Evaluate the model on testing set
score = classifier_cnn.evaluate(testing_set)

# Print test accuracy
print('\n', 'Test accuracy:', score[1])

from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score

# Retrieve a batch of images from the test set
image_batch, label_batch = testing_set.as_numpy_iterator().next()
predictions = classifier_cnn.predict_on_batch(image_batch).flatten()

# Apply a sigmoid since our model returns logits
predictions = tf.nn.sigmoid(predictions)
predictions = (predictions>0.5)

a = accuracy_score(predictions, label_batch)
print('Accuracy is:', a*100)

print('Predictions:\n', predictions.numpy())
print('Labels:\n', label_batch)

class_names = testing_set.class_names

plt.figure(figsize=(10, 10))
for i in range(9):
  ax = plt.subplot(3, 3, i + 1)
  plt.imshow(image_batch[i].astype("uint8"))
  plt.title(class_names[predictions[i]])
  plt.axis("off")