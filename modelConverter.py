import tensorflow as tf

# Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model('Tensorflow_models/mobileNetV2_10_epochs_fine_tune_10_1') # path to the SavedModel directory
tflite_model = converter.convert()

# Save the model.
with open('Tensorflow_lite_models/model.tflite', 'wb') as f:
  f.write(tflite_model)