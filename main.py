import tensorflow as tf
tf.test.gpu_device_name()

# Set datasets
BATCH_SIZE = 128
IMG_SIZE = (64,64)

train_dataset = tf.keras.utils.image_dataset_from_directory('./dataset/Training',
                                                            shuffle=True,
                                                            batch_size=BATCH_SIZE,
                                                            image_size=IMG_SIZE)

validation_dataset = tf.keras.utils.image_dataset_from_directory('./dataset/Validation',
                                                            shuffle=True,
                                                            batch_size=BATCH_SIZE,
                                                            image_size=IMG_SIZE)

test_dataset = tf.keras.utils.image_dataset_from_directory('./dataset/Testing',
                                                            shuffle=True,
                                                            batch_size=BATCH_SIZE,
                                                            image_size=IMG_SIZE)

# Show number of batches in datasets
print('Number of training batches: %d' % tf.data.experimental.cardinality(train_dataset))
print('Number of validation batches: %d' % tf.data.experimental.cardinality(validation_dataset))
print('Number of test batches: %d' % tf.data.experimental.cardinality(test_dataset))


# Part 1 : building the CNN
# Importing the layers
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, RandomFlip, RandomRotation

# Initialize the CNN
classifier = Sequential()

classifier.add(Conv2D(filters=32, kernel_size=(3,3), activation='relu', input_shape=(64,64,3)))
classifier.add(MaxPooling2D(pool_size=(2,2)))
classifier.add(Conv2D(filters=32, kernel_size=(3,3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2,2)))
classifier.add(Flatten())
classifier.add(Dense(units=128, activation='relu')) # Hidden layer with 128 neurons
classifier.add(Dense(units=1, activation='sigmoid')) # Output layer (Binary Classification so 1 output + sigmoid)

# Compiling the CNN
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
classifier.summary()

# Part 2 : Getting the data

# Part 3 : Fitting the CNN to the images
from keras_preprocessing.image import ImageDataGenerator

batch_size=128
img_height=64
img_width=64
epochs=25

train_datagen = ImageDataGenerator(rescale=1./255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory(
    './dataset/Training',
    target_size=(img_height,img_width), # size of images
    batch_size=batch_size,
    class_mode='binary'
)

test_set = test_datagen.flow_from_directory(
    './dataset/Testing',
    target_size=(img_height,img_width), # size of images
    batch_size=batch_size,
    class_mode='binary'
)

history=classifier.fit(training_set,
               epochs=epochs,
               validation_data=test_set)


import matplotlib.pyplot as plt

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

classifier.save('cnn_25_epochs_1.h5')

# Testing the model
validation_set = tf.keras.utils.image_dataset_from_directory('./dataset/Validation',
                                                                 shuffle=True,
                                                                 batch_size=batch_size,
                                                                 image_size=(img_height,img_width))
# Evaluate the model on validation set
score=classifier.evaluate(validation_set)

# Print test accuracy
print('\n', 'Test accuracy:', score[1])

# Prediction
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score

# Retrieve a batch of images from the test set
image_batch, label_batch = validation_set.as_numpy_iterator().next()
predictions = classifier.predict_on_batch(image_batch).flatten()

# Apply a sigmoid since our model returns logits
predictions = tf.nn.sigmoid(predictions)
predictions = (predictions>0.5)

a = accuracy_score(predictions, label_batch)
print('Accuracy is:', a*100)

print('Predictions:\n', predictions.numpy())
print('Labels:\n', label_batch)

class_names=validation_set.class_names

plt.figure(figsize=(10, 10))
for i in range(9):
  ax = plt.subplot(3, 3, i + 1)
  plt.imshow(image_batch[i].astype("uint8"))
  plt.title(class_names[predictions[i]])
  plt.axis("off")


# Making the confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(label_batch,predictions)

import seaborn as sns
#Visual Confusion matrix
plt.figure(figsize=(9,9))
sns.heatmap(cm, annot=True, fmt=".3f", linewidths=.5, square=True, cmap='Blues_r')
plt.ylabel('Actual label')
plt.xlabel('Predicted label')
plt.show()